/turf/unsimulated/teleporter
	name = "Gateway"
	icon = 'icons/invis.dmi'
	icon_state = "invis"
	blocks_air = 1
	density = 1
	opacity = 1
	var/tele_x
	var/tele_y
	var/tele_z

/turf/unsimulated/teleporter/Bumped(var/mob/A)
	A.x = tele_x
	A.y = tele_y
	A.z = tele_z

/turf/unsimulated/teleporter/attack_ghost(mob/observer/ghost/user as mob)
	user.x = tele_x
	user.y = tele_y
	user.z = tele_z